#include "entername_window.h"
#include "ui_entername_window.h"

Entername_Window::Entername_Window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Entername_Window)
{
    ui->setupUi(this);
}

Entername_Window::~Entername_Window()
{
    delete ui;
}


void Entername_Window::on_name_textEdited(const QString &arg1)
{
    Entername_Window::m_entername = arg1;
}

QString Entername_Window::getName()
{
    return Entername_Window::m_entername;
}

int Entername_Window::getReadStatus(){
    return Entername_Window::button_read;
}


int Entername_Window::getEditStatus(){
    return Entername_Window::button_edit;
}


void Entername_Window::on_enter_clicked()
{
    emit enter_button_clicked();
}

void Entername_Window::on_read_stateChanged(int arg1)
{
    Entername_Window::button_read = arg1;
}

void Entername_Window::on_edit_stateChanged(int arg1)
{
    Entername_Window::button_edit = arg1;
}
