#ifndef REG_WINDOW_H
#define REG_WINDOW_H

#include <QWidget>
#include <QDialog>

namespace Ui {
class Reg_window;
}

class Reg_window : public QDialog
{
    Q_OBJECT

public:
    explicit Reg_window(QWidget *parent = 0);
    ~Reg_window();
    QString getName();
    QString getPass();
    bool checkPass();

signals:
    void register_button_clicked2();
    void back_button_clicked();

private slots:
    void on_namelineEdit_textEdited(const QString &arg1);

    void on_passwordlineEdit_textEdited(const QString &arg1);

    void on_confirmlineEdit_3_textEdited(const QString &arg1);

    void on_RegisterPushButton_clicked();

    void on_back_clicked();

private:
    Ui::Reg_window *ui;
    QString m_userName;
    QString m_userPass;
    QString m_confirmation;
};

#endif // REG_WINDOW_H
