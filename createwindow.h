#ifndef CREATEWINDOW_H
#define CREATEWINDOW_H

#include <QWidget>
#include <QDialog>
#include <QDir>
#include <QFileSystemModel>

namespace Ui {
class CreateWindow;
}

class CreateWindow : public QWidget
{
    Q_OBJECT

public:
    explicit CreateWindow(QWidget *parent = 0);
    ~CreateWindow();
    QString getFilename();
    void setDir(QDir dirname);
    QDir getDir();
    QDir dir_file;

signals:
    void pushcreate_button_clicked();
    void closeButton_button_clicked();

private slots:
    void on_namefile_textEdited(const QString &arg1);

    void on_pushcreate_clicked();

    void on_closeButton_clicked();

private:
    Ui::CreateWindow *ui;
    QString name_file;
};

#endif // CREATEWINDOW_H
