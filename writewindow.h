#ifndef WRITEWINDOW_H
#define WRITEWINDOW_H

#include <QWidget>

namespace Ui {
class WriteWindow;
}

class WriteWindow : public QWidget
{
    Q_OBJECT

public:
    explicit WriteWindow(QWidget *parent = 0);
    ~WriteWindow();
    QString getInputText();

signals:
    void ok_button_clicked();
    void cancel_button_clicked();

private slots:
    void on_lineEdit_textEdited(const QString &arg1);


    void on_ok_clicked();
    void on_cancel_clicked();

private:
    Ui::WriteWindow *ui;
    QString input;
};

#endif // WRITEWINDOW_H
