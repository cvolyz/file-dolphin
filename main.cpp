#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("File Dolphin");
    MainWindow mainWindow;
    mainWindow.display();
    return a.exec();
}
