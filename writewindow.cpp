#include "writewindow.h"
#include "ui_writewindow.h"

WriteWindow::WriteWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WriteWindow)
{
    ui->setupUi(this);
}

WriteWindow::~WriteWindow()
{
    delete ui;
}

void WriteWindow::on_lineEdit_textEdited(const QString &arg1)
{
    WriteWindow::input = arg1;
}

QString WriteWindow::getInputText()
{
    return WriteWindow::input;
}

void WriteWindow::on_ok_clicked()
{
    emit ok_button_clicked();
}

void WriteWindow::on_cancel_clicked()
{
    emit cancel_button_clicked();
}
