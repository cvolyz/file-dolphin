#ifndef ENTERNAME_WINDOW_H
#define ENTERNAME_WINDOW_H

#include <QDialog>
#include <QDir>
#include <QFileSystemModel>
#include <QWidget>

namespace Ui {
class Entername_Window;
}

class Entername_Window : public QDialog
{
    Q_OBJECT

public:
    explicit Entername_Window(QWidget *parent = 0);
    ~Entername_Window();

    QString getName();
    int getReadStatus();
    int getEditStatus();

signals:
    void enter_button_clicked();


private slots:
    void on_name_textEdited(const QString &arg1);
    void on_enter_clicked();

    void on_read_stateChanged(int arg1);
    void on_edit_stateChanged(int arg1);

private:
    Ui::Entername_Window *ui;
    QString m_entername;
    int button_read;
    int button_edit;
};

#endif // ENTERNAME_WINDOW_H
