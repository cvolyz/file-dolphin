#include "createwindow.h"
#include "ui_createwindow.h"

CreateWindow::CreateWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CreateWindow)
{
    ui->setupUi(this);
}

CreateWindow::~CreateWindow()
{
    delete ui;
}

void CreateWindow::on_namefile_textEdited(const QString &arg1)
{
    CreateWindow::name_file = arg1;
}

QString CreateWindow::getFilename(){
    return CreateWindow::name_file;
}

void CreateWindow::setDir(QDir dirname){
    CreateWindow::dir_file = dirname;
}

QDir CreateWindow::getDir(){
    return CreateWindow::dir_file;
}

void CreateWindow::on_pushcreate_clicked()
{
    emit pushcreate_button_clicked();
}

void CreateWindow::on_closeButton_clicked()
{
    emit closeButton_button_clicked();
}
