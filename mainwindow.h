#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QString>
#include <QtSql/QtSql>
#include "auth_window.h"
#include "reg_window.h"
#include "entername_window.h"
#include <QDir>
#include <QWidget>
#include <QFileSystemModel>
#include "createwindow.h"
#include "writewindow.h"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void display(); // прототип пользовательской функции отображения
    bool connectDB(); // прототип метода подключения к БД
    QString filename;

signals:
    void logout_button_clicked();
    void changeaccess_button_clicked();
    void create_button_clicked();
    void delete_2_button_clicked();
    void read_button_clicked();
    void help_button_clicked();

private:
    Ui::MainWindow *ui_Main;
    QFileSystemModel *model;

    Auth_Window ui_Auth; // экземпляры окна авторизации и окна регистрации
    Reg_window ui_Reg; // принадлежат главному окну
    Entername_Window ui_EN;
    CreateWindow ui_CW;
    WriteWindow ui_WW;

    QString m_username; // строки для обработки
    QString m_userpass; // пользовательского ввода

    QString db_input; // строка для отправки запроса к БД

    QSqlDatabase mw_db; // экземпляр подключения к БД

    int user_counter; // счетчик пользователей
    bool m_loginSuccesfull; // флаг успешной авторизации

private slots:
    void authorizeUser(); // пользовательские слоты
    void authorizeWindowShow();
    void registerWindowShow();
    void registerUser();
    void enternameWindow();
    void changeaccessWindow();
    void createWindow();
    void createFilewindow();
    void deleteWindow();
    void readfileWindow();
    void writeWindow();
    void closeCWWindow();
    void closeWWWindow();
    void helpWindow();

    void on_logout_clicked();
    void on_changeaccess_clicked();

    void on_listView_doubleClicked(const QModelIndex &index);
    void on_listView_clicked(const QModelIndex &index);
    void on_create_clicked();
    void on_delete_2_clicked();
    void on_readfile_clicked();
    void on_help_clicked();
};


#endif // MAINWINDOW_H
