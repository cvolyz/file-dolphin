#include "mainwindow.h"
#include <QDesktopWidget>
#include <QMessageBox>
#include <QtSql/QSqlDatabase>
#include "ui_mainwindow.h"
#include "auth_window.h"
#include "reg_window.h"
#include "entername_window.h"
#include "createwindow.h"
#include "writewindow.h"
#include <QtDebug>
#include <QFile>
#include <string.h>

MainWindow::MainWindow(QWidget *parent) :
QMainWindow(parent),
    ui_Main(new Ui::MainWindow)
{
    //setWindowTitle(tr("File Dolphin"));
    //resize(QDesktopWidget().availableGeometry(this).size() * 0.6);


    user_counter = 0;
    m_loginSuccesfull = false;
    connect(&ui_Auth, SIGNAL(login_button_clicked()),
            this, SLOT(authorizeUser()));
    connect(&ui_Auth,SIGNAL(destroyed()),
            this, SLOT(show()));
    connect(&ui_Auth,SIGNAL(register_button_clicked()),
            this,SLOT(registerWindowShow()));
    connect(&ui_Reg,SIGNAL(register_button_clicked2()),
            this,SLOT(registerUser()));
    connect(&ui_Reg, SIGNAL(back_button_clicked()),
            this, SLOT(authorizeWindowShow()));
    connect(&ui_Reg,SIGNAL(destroyed()),
            &ui_Auth, SLOT(show()));
    connect(this, SIGNAL(logout_button_clicked()),
            this, SLOT(authorizeWindowShow()));
    connect(this, SIGNAL(changeaccess_button_clicked()),
            this, SLOT(enternameWindow()));
    connect(this, SIGNAL(create_button_clicked()),
            this, SLOT(createWindow()));
    connect(&ui_EN, SIGNAL(enter_button_clicked()),
            this, SLOT(changeaccessWindow()));
    connect(&ui_CW, SIGNAL(pushcreate_button_clicked()),
            this, SLOT(createFilewindow()));
    connect(&ui_CW, SIGNAL(closeButton_button_clicked()),
            this, SLOT(closeCWWindow()));
    connect(this, SIGNAL(delete_2_button_clicked()),
            this, SLOT(deleteWindow()));
    connect(this, SIGNAL(read_button_clicked()),
            this, SLOT(readfileWindow()));
    connect(&ui_WW,SIGNAL(ok_button_clicked()),
            this, SLOT(writeWindow()));
    connect(&ui_WW, SIGNAL(cancel_button_clicked()),
            this, SLOT(closeWWWindow()));
    connect(this, SIGNAL(help_button_clicked()),
            this, SLOT(helpWindow()));

    if(!connectDB())
    {
        qDebug() << "Failed to connect DB";
    }

    QSqlQuery query;

    db_input = "CREATE TABLE userlist ( "
               "number INTEGER PRIMARY KEY NOT NULL,"
               "name VARCHAR(20), "
               "pass VARCHAR(20), "
               "reader BOOLEAN, "
               "editorr BOOLEAN, "
               "xpos INTEGER, "
               "ypos INTEGER, "
               "width INTEGER, "
               "length INTEGER );";
    if(!query.exec(db_input))
    {
            qDebug() << "File manager is activated!";
    }

    QSqlRecord rec;
    QString str_t = "SELECT COUNT(*) "
                    "FROM userlist;";
    db_input = str_t;
    if(!query.exec(db_input))
    {
        qDebug() << "Unable to get number " << query.lastError() << " : " << query.lastQuery();
    }
    else
    {
        query.next();
        rec = query.record();
        user_counter = rec.value(0).toInt();
    }

    if(!user_counter){
        QString str_t = "INSERT INTO userlist(number, name, pass, reader, editorr, xpos, ypos, width, length) "
                          "VALUES(%1, '%2', '%3', %4, %5, %6, %7, %8, %9);";
        db_input = str_t.arg(0)
                        .arg("admin")
                        .arg("admin")
                        .arg(2)
                        .arg(2)
                        .arg(0)
                        .arg(0)
                        .arg(800)
                        .arg(400);
         if(!query.exec(db_input))
         {
             qDebug() << "Unable to insert data"  << query.lastError() << " : " << query.lastQuery();
         }
    }
    ui_Main->setupUi(this);


    model = new QFileSystemModel(this);
    model -> setFilter(QDir::QDir::AllEntries);
    model -> setRootPath("");
    ui_Main -> listView -> setModel(model) ;
}

void MainWindow::authorizeWindowShow()
{
    this->hide();
    ui_Reg.hide();
    ui_Auth.show();
}


void MainWindow::authorizeUser()
{
    m_username = ui_Auth.getLogin();
    m_userpass = ui_Auth.getPass();

    if(m_username.isEmpty())
        qDebug() << "Enter login";

    if(m_userpass.isEmpty())
        qDebug() << "Enter password";

    else{

        QString str_t = " SELECT * "
                        " FROM userlist "
                        " WHERE name = '%1' ";
        // int db_number = 0;

        QString username = "";
        QString userpass = "";

        int xPos = 0;
        int yPos = 0;

        int width = 0;
        int length = 0;

        db_input = str_t.arg(m_username);

        QSqlQuery query;
        QSqlRecord rec;

        if(!query.exec(db_input)){
            qDebug() << "Unable to execute query - exiting" << query.lastError() << " : " << query.lastQuery();
        }
        rec = query.record();
        query.next();
        user_counter = query.value(rec.indexOf("number")).toInt();
        username = query.value(rec.indexOf("name")).toString();
        userpass = query.value(rec.indexOf("pass")).toString();
        if(m_username != username){
            qDebug() << m_username << "missmatch";
            m_loginSuccesfull = false;
        }
        else if(m_userpass != userpass){
            qDebug() << m_userpass << " is not password for " << m_username;
            m_loginSuccesfull = false;
        }
        else
        {
            m_loginSuccesfull = true;
            xPos = query.value(rec.indexOf("xpos")).toInt();
            yPos = query.value(rec.indexOf("ypos")).toInt();
            width = query.value(rec.indexOf("width")).toInt();
            length = query.value(rec.indexOf("length")).toInt();
            ui_Auth.close();
            ui_Reg.close();
            this->setGeometry(xPos,yPos,width, length);
            this->setWindowTitle(tr("File Dolphin"));
            this->resize(QDesktopWidget().availableGeometry(this).size() * 0.6);
            this->show();

        }
    }
}

void MainWindow::enternameWindow(){
    if(ui_Auth.getLogin() != "admin")
        {
        QMessageBox::information(this, "Error", "You are not admin");
    }
    else{
        ui_EN.setWindowTitle(tr("Change access"));
        ui_EN.show();
    }
}

void MainWindow::createWindow(){
    if(ui_Auth.getLogin() != "admin")
        {
        QMessageBox::information(this, "Error", "You are not admin");
    }
    else{
        ui_CW.setWindowTitle(tr("Create"));
        ui_CW.show();
    }
}

void MainWindow::deleteWindow(){
    if(ui_Auth.getLogin() != "admin")
        {
        QMessageBox::information(this, "Error", "You are not admin");
    }
    else{
        QDir direct = ui_CW.getDir();
        QFile::remove(direct.path()+"/"+filename);
    }
}

void MainWindow::createFilewindow(){
    QString newfile = ui_CW.getFilename()+".txt";
    QDir direct = ui_CW.getDir();
    QFile file(direct.path()+"/"+newfile);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        file.write("");
        file.close();
    }
    ui_CW.close();
}

void MainWindow::readfileWindow(){
    QSqlQuery query;
    QString str_t = "SELECT * "
                    "FROM userlist "
                    "WHERE name = '%1' ";
    db_input = str_t.arg(ui_Auth.getLogin());
    if(!query.exec(db_input)){
        qDebug() << "Unable to execute query - exiting " << query.lastError() << " : " << query.lastQuery();
    }
    query.next();
    QSqlRecord rec = query.record();
    int read_access = query.value(rec.indexOf("reader")).toInt();
    if(read_access == 2){
        QDir direct = ui_CW.getDir();
        QString file_name = direct.path()+"/"+filename;
        QFile file_1(file_name);
        if ((QFile::exists(file_name))&&(file_1.open(QIODevice::ReadOnly))){
            qDebug() << QString(file_1.readAll());
            file_1.close();
        }
     }
     else
        QMessageBox::information(this, "Error", "Read access is closed");
}

void MainWindow::writeWindow(){
    QDir direct = ui_CW.getDir();
    QString file_name = direct.path()+"/"+filename;
    QFile file_1(file_name);
    QString str = ui_WW.getInputText();
    if ((QFile::exists(file_name))&&(file_1.open(QIODevice::WriteOnly))){
        file_1.write(str.toUtf8());
        file_1.close();
    }
    ui_WW.close();
}

void MainWindow::changeaccessWindow()
{   QSqlQuery query;
    QString str_t = "SELECT * "
                    "FROM userlist "
                    "WHERE name = '%1' ";
    db_input = str_t.arg(ui_EN.getName());

    if(!query.exec(db_input)){
        qDebug() << "Unable to execute query - exiting " << query.lastError() << " : " << query.lastQuery();
    }
    query.next();
    QSqlRecord rec = query.record();
    QString username = query.value(rec.indexOf("name")).toString();
    if(username.isEmpty()){
        qDebug() << "The user with name" << ui_EN.getName() << "is not registered";
    }
    else{
        int read = ui_EN.getReadStatus();
        int edit = ui_EN.getEditStatus();
        str_t = "UPDATE userlist "
                "SET reader = %2, editorr = %3 "
                "WHERE name = '%1';";
        db_input = str_t .arg(ui_EN.getName())
                         .arg(read)
                         .arg(edit);
        if(!query.exec(db_input)){
            qDebug() << "Unable to execute query - exiting " << query.lastError() << " : " << query.lastQuery();
            return;
                }
        qDebug() << "The updates were successful!";
        ui_EN.close();
    }
}

void MainWindow::helpWindow(){
    QMessageBox::information(this, "Help", "For help, contact the support service");
}

void MainWindow::registerWindowShow()
{
    ui_Auth.hide();
    ui_Reg.setWindowTitle(tr("Registration"));
    ui_Reg.show();
}

void MainWindow::registerUser()
{   m_username = ui_Reg.getName();
    QSqlQuery query;
    QSqlRecord rec;
    QString str_t = "SELECT * "
                    "FROM userlist "
                    "WHERE name = '%1' ";
    db_input = str_t.arg(m_username);


    if(!query.exec(db_input))
    {
        qDebug() << "Unable to get number " << query.lastError() << " : " << query.lastQuery();
    }
    query.next();
    rec = query.record();
    QString username = query.value(rec.indexOf("name")).toString();
    if(username.isEmpty()){
        if(ui_Reg.getPass().isEmpty()){
            qDebug() << "Enter password";
            ui_Reg.show();
        }
        else{
            if(ui_Reg.checkPass()){

                str_t = "SELECT COUNT(*) "
                        "FROM userlist;";
                db_input = str_t;
                if(!query.exec(db_input))
                {
                    qDebug() << "Unable to get number " << query.lastError() << " : " << query.lastQuery();
                    return;
                }
                else
                {
                    query.next();
                    rec = query.record();
                    user_counter = rec.value(0).toInt();
                    qDebug() << user_counter;
                }

                m_userpass = ui_Reg.getPass();
                user_counter++;
                str_t = "INSERT INTO userlist(number, name, pass, reader, editorr, xpos, ypos, width, length) "
                        "VALUES(%1, '%2', '%3', %4, %5, %6, %7, %8, %9);";
                db_input = str_t.arg(user_counter)
                                .arg(m_username)
                                .arg(m_userpass)
                                .arg(0)
                                .arg(0)
                                .arg(0)
                                .arg(0)
                                .arg(800)
                                .arg(400);

                if(!query.exec(db_input))
                {
                    qDebug() << "Unable to insert data"  << query.lastError() << " : " << query.lastQuery();
                }
                else
                {
                    ui_Reg.hide();
                    ui_Auth.show();
                }
             }
            else
                qDebug() << "Confirm password corectly";
        }
    }
    else{
        qDebug() << "The user with name" << username << "is already registered";
        ui_Reg.show();
    }

}

void MainWindow::closeCWWindow(){
    ui_CW.close();
}

void MainWindow::closeWWWindow(){
    ui_WW.close();
}


void MainWindow::display()
{
    ui_Auth.setWindowTitle(tr("Authorization"));
    ui_Auth.show();
}


MainWindow::~MainWindow()
{
    if(m_loginSuccesfull)
    {
        QString str_t = "UPDATE userlist "
                        "SET xpos = %2, ypos = %3, width = %4, length = %5 "
                        "WHERE name = '%1';";
        db_input = str_t .arg(m_username)
                         .arg(this->x())
                         .arg(this->y())
                         .arg(this->width())
                         .arg(this->height());
        QSqlQuery query;
        if(!query.exec(db_input))
        {
            qDebug() << "Unable to insert data"  << query.lastError() << " : " << query.lastQuery() ;
        }
    }
    mw_db.removeDatabase("database.sqlite");
    qDebug() << "MainWindow Destroyed";
    delete ui_Main;
    exit(0);
}


bool MainWindow::connectDB()
{
    mw_db = QSqlDatabase::addDatabase("QSQLITE");
    mw_db.setDatabaseName("database.sqlite");
    if(!mw_db.open())
    {
        qDebug() << "Cannot open database: " << mw_db.lastError();
        return false;
    }

    return true;
}


void MainWindow::on_logout_clicked()
{
    emit logout_button_clicked();
}

void MainWindow::on_changeaccess_clicked()
{
    emit changeaccess_button_clicked();
}



void MainWindow::on_listView_doubleClicked(const QModelIndex &index)
{
    QListView* ListView = (QListView*) sender();
    QFileInfo fileInfo = model -> fileInfo(index);
    QDir dir = fileInfo.dir();
    if (fileInfo.fileName() == "..")
    {
        dir = fileInfo.dir();
        dir.cdUp();
        ListView -> setRootIndex( model -> index(dir.absolutePath()));
    }
    else if(fileInfo.fileName() == ".")
    {
        ListView -> setRootIndex(model -> index(""));
    }
    else if(fileInfo.isDir())
    {
        ListView -> setRootIndex(index);
    }
    else{
        QSqlQuery query;
        QString str_t = "SELECT * "
                        "FROM userlist "
                        "WHERE name = '%1' ";
        db_input = str_t.arg(ui_Auth.getLogin());
        if(!query.exec(db_input)){
            qDebug() << "Unable to execute query - exiting " << query.lastError() << " : " << query.lastQuery();
        }
        query.next();
        QSqlRecord rec = query.record();
        int write_access = query.value(rec.indexOf("editorr")).toInt();
        if(write_access == 2){
            ui_WW.setWindowTitle("Write window");
            ui_WW.show();
        }
        else
            QMessageBox::information(this, "Error", "Write access is closed");
    }
}

void MainWindow::on_create_clicked()
{
    emit create_button_clicked();
}

void MainWindow::on_delete_2_clicked()
{
    emit delete_2_button_clicked();
}

void MainWindow::on_listView_clicked(const QModelIndex &index)
{
    QListView* ListView = (QListView*) sender();
    QFileInfo fileInfo = model -> fileInfo(index);
    filename = fileInfo.fileName();
    QDir dir = fileInfo.dir();
    ui_CW.setDir(dir);
}

void MainWindow::on_readfile_clicked()
{
    emit read_button_clicked();
}

void MainWindow::on_help_clicked()
{
    emit help_button_clicked();
}
