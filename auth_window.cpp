#include "auth_window.h"
#include "ui_auth_window.h"

Auth_Window::Auth_Window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Auth_Window)
{
    ui->setupUi(this);
}

Auth_Window::~Auth_Window()
{
    delete ui;
}

void Auth_Window::on_lineEditName_textEdited(const QString &arg1)
{
    Auth_Window::m_username = arg1;
}

void Auth_Window::on_lineEditPassword_textEdited(const QString &arg1)
{
    Auth_Window::m_userpass = arg1;
}

void Auth_Window::on_pushButtonLogin_clicked()
{
    emit login_button_clicked();
}

void Auth_Window::on_pushButtonRegistr_clicked()
{
    emit register_button_clicked();
}

QString Auth_Window::getLogin()
{
    return Auth_Window::m_username;
}

QString Auth_Window::getPass()
{
    return Auth_Window::m_userpass;
}
