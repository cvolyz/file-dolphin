#include "reg_window.h"
#include "ui_reg_window.h"

Reg_window::Reg_window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Reg_window)
{
    ui->setupUi(this);
}

Reg_window::~Reg_window()
{
    delete ui;
}

void Reg_window::on_confirmlineEdit_3_textEdited(const QString &arg1)
{
    Reg_window::m_confirmation = arg1;
}

void Reg_window::on_namelineEdit_textEdited(const QString &arg1)
{
    Reg_window::m_userName = arg1;
}

void Reg_window::on_passwordlineEdit_textEdited(const QString &arg1)
{
    Reg_window::m_userPass = arg1;
}

void Reg_window::on_RegisterPushButton_clicked()
{
    emit register_button_clicked2();
}


void Reg_window::on_back_clicked()
{
    emit back_button_clicked();
}

QString Reg_window::getName()
{
    return m_userName;
}

QString Reg_window::getPass()
{
    return m_userPass;
}

bool Reg_window::checkPass()
{
    return (m_confirmation == m_userPass);
}
